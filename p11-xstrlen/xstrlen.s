
	.text
	.global xstrlen
xstrlen:
	movl  4(%esp), %ecx
ini:
	movb  (%ecx), %al
	inc   %ecx
	testb %al, %al
	jnz   ini
done:
    subl  4(%esp), %ecx  # %ecx -= $str
    dec   %ecx           # %ecx -= 1
    movl  %ecx, %eax
    ret
