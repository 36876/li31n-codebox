
	.text
	.global xputs
xputs:
	pushl  %ebp
	movl   %esp, %ebp
	pushl  %edi
	subl   $4, %esp

	movl   8(%ebp), %edi
	jmp    check
next:
	movl   %ecx, 0(%esp)
	call   putchar
	incl   %edi
check:
	movzbl (%edi), %ecx
	testb  %cl, %cl
	jnz    next
done:
	movl  $0x0A, 0(%esp)
	call   putchar

	addl   $4, %esp
	popl   %edi
	popl   %ebp
	ret
