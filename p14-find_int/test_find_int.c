#include <stdio.h>
#include <stdlib.h>

int find_int(int * items,
             unsigned len,
             int ref,
             int (*best)(int val, int ref));

int min2(int a, int b);
int max2(int a, int b);

int min_of(int * data, unsigned len) {
	return find_int(data, len, 0x7fffffff, &min2);
}

int max_of(int * data, unsigned len) {
	return find_int(data, len, 0x80000000, &max2);
}

int closest_to_3(int a, int b) {
	return (abs(a - 3) < abs(b - 3)) ? a : b;
}

int ct3_of(int * data, unsigned len) {
	return find_int(data, len, 0x80000002, &closest_to_3);
}

int main() {

	int items[] = { 7, 0, -3, 11, -12, 1 };
	
	int min = min_of(items, 6);
	int max = max_of(items, 6);
	int ct3 = ct3_of(items, 6);
	
	printf("min = %d | max = %d | ct3 = %d\n", min, max, ct3);

	return 0;
}
