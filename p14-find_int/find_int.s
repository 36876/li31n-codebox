
	.text
	
	.globl min2
min2:
	movl  4(%esp), %eax
	cmpl  8(%esp), %eax
	jle   1f
	movl  8(%esp), %eax
1:	ret
	
	.globl max2
max2:
	movl  4(%esp), %ecx
	cmpl  8(%esp), %ecx
	jge   1f
	movl  8(%esp), %ecx
	movl  %ecx, %eax
1:	ret

	.globl find_int

find_int:
	pushl  %ebp
	movl   %esp, %ebp
	pushl  %esi
	pushl  %ebx
	
	movl   16(%ebp), %eax     # eax: mínimo encontrado
	movl   12(%ebp), %ebx     # ebx: quantos faltam
	movl   8(%ebp), %esi      # esi: ponteiro para posição corrente

next:
	movl   -4(%esi, %ebx, 4), %edx   # edx: valor corrente

	pushl  %edx
	pushl  %eax
	call   *20(%ebp)
	addl   $8, %esp

	decl   %ebx
	jnz    next

	popl   %ebx
	popl   %esi
	popl   %ebp
	ret
