
	.text
	
	.globl min_of
	
min_of:
	pushl  %ebp
	movl   %esp, %ebp
	pushl  %esi
	
	movl   $0x7FFFFFFF, %eax  # eax: mínimo encontrado
	movl   12(%ebp), %ecx     # ecx: quantos faltam
	movl   8(%ebp), %esi      # esi: ponteiro para posição corrente

next:
	movl   (%esi), %edx       # edx: valor corrente
	cmpl   %eax, %edx
	jge    not_min
	movl   %edx, %eax
not_min:
	addl   $4, %esi
	decl   %ecx
	jnz    next

	popl   %esi
	popl   %ebp
	ret
